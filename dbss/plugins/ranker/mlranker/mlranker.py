#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pickle
import pkgutil

from dbss.core.ranker import Ranker
from dbss.plugins.ranker.mlranker import feature_extractor

## Implements Ranker abstract class from dbss/core/ranker.py.
## Outputs the probability the classifier gives for the 'Parsed' class.


with open('dbss/plugins/ranker/mlranker/models/me_classifier3.pickle', 'rb') as pf:
    classifier = pickle.load(pf)


class MLRanker(Ranker):

    ## (listof/generator (Sentence, Sentence, (listof String))) -> (generator (Sentence, Sentence, Float))
    def score(self, input):
        for original, simplified, actions in input:
            yield original, \
                  simplified, \
                  classifier.prob_classify(feature_extractor.extract_features(original, simplified, actions)).prob('Parsed')
