#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from dbss.core import sentence
from dbss.core.ranker import Ranker

## Implements Ranker abstract class from dbss/core/ranker.py.


class ByLengthRanker(Ranker):

    ## (listof/generator (Sentence, Sentence, (listof String))) -> (generator (Sentence, Sentence, Float))
    def score(self, input):
        for original, simplified, actions in input:
            yield original, simplified, len(simplified)
