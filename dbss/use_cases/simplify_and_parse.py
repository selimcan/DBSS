"""
See the 'Use Cases' section of this doc: https://www.overleaf.com/read/rqzsvfrwctft
"""

import spacy
from multiprocessing import Queue

from dbss import config
from dbss.core import sentence, simplifier


## =================
## Data definitions:


class Over(object):
    pass
## interp. sentinel used for shutting down components reading from a queue


## ==========
## Constants:


## dependency parser
NLP = spacy.load('de')


## ==========
## Functions:


## String -> Sentence
def string2sentence(plain_s):
    """Parse the string with a dependency parser and return a Sentence."""
    doc = NLP(plain_s)
    return tuple(sentence.t.Token(str(t.i),
                                  t.orth_,
                                  t.lemma_,
                                  '_',
                                  t.tag_,
                                  '_',
                                  '_',
                                  '_',
                                  str(t.head.i),
                                  '_',
                                  t.dep_.upper(),
                                  '_',
                                  '_',
                                  '_',
                                  '_') for t in doc)

def test_string2sentence():
    assert string2sentence(sentence.s2string(sentence.S_5)) == \
           (sentence.t.Token("0", "Ein", "ein", "_", "ART", "_", "_",
                    "_", "1", "_", "NK", "_", "_", "_", "_"),
            sentence.t.Token("1", "Sprecher", "sprecher", "_", "NN", "_", "_",
                    "_", "5", "_", "SB", "_", "_", "_", "_"),
            sentence.t.Token("2", "des", "des", "_", "ART", "_", "_",
                    "_", "4", "_", "NK", "_", "_", "_", "_"),
            sentence.t.Token("3", "japanischen", "japanischen", "_", "ADJA", "_", "_",
                    "_", "4", "_", "NK", "_", "_", "_", "_"),
            sentence.t.Token("4", "Außenministerium", "außenministerium", "_", "NN", "_", "_",
                    "_", "1", "_", "AG", "_", "_", "_", "_"),
            sentence.t.Token("5", "verkündete", "verkündete", "_", "VVFIN", "_", "_",
                    "_", "5", "_", "ROOT", "_", "_", "_", "_"),
            sentence.t.Token("6", "daraufhin", "daraufhin", "_", "PROAV", "_", "_",
                    "_", "5", "_", "MO", "_", "_", "_", "_"),
            sentence.t.Token("7", ",", ",", "_", "$,", "_", "_", "_", "7", "_", "PUNCT", "_", "_", "_", "_"),
            sentence.t.Token("8", "man", "man", "_", "PIS", "_", "_",
                    "_", "9", "_", "SB", "_", "_", "_", "_"),
            sentence.t.Token("9", "werde", "werde", "_", "VAFIN", "_", "_",
                    "_", "5", "_", "OC", "_", "_", "_", "_"),
            sentence.t.Token("10", "Jelzins", "jelzins", "_", "NE", "_", "_",
                    "_", "11", "_", "AG", "_", "_", "_", "_"),
            sentence.t.Token("11", "Aussage", "aussage", "_", "NN", "_", "_",
                    "_", "14", "_", "OA", "_", "_", "_", "_"),
            sentence.t.Token("12", "``", "``", "_", "$(", "_", "_", "_", "15", "_", "PUNCT", "_", "_", "_", "_"),
            sentence.t.Token("13", "vorsichtig", "vorsichtig", "_", "ADJD", "_", "",
                    "_", "14", "_", "MO", "_", "_", "_", "_"),
            sentence.t.Token("14", "analysieren", "analysieren", "_", "VVINF", "_", "_",
                    "_", "9", "_", "OC", "_", "_", "_", "_"),
            sentence.t.Token("15", "''", "--", "_", "$(", "_", "_",
                    "_", "14", "_", "PUNCT", "_", "_", "_", "_"),
            sentence.t.Token("16", ",", ",", "_", "$,", "_", "_",
                    "_", "14", "_", "PUNCT", "_", "_", "_", "_"),
            sentence.t.Token("17", "bevor", "bevor", "_", "KOUS", "_", "_",
                    "_", "20", "_", "CP", "_", "_", "_", "_"),
            sentence.t.Token("18", "man", "man", "_", "PIS", "_", "_",
                    "_", "20", "_", "SB", "_", "_", "_", "_"),
            sentence.t.Token("19", "sie", "sie", "_", "PPER", "_", "_",
                    "_", "20", "_", "OA", "_", "_", "_", "_"),
            sentence.t.Token("20", "kommentiere", "kommentiere", "_", "VVFIN", "_",
                    "_", "_", "14", "_", "MO", "_", "_", "_", "_"),
            sentence.t.Token("21", ",", ",", "_", "$,", "_", "_",
                    "_", "14", "_", "PUNCT", "_", "_", "_", "_"),
            sentence.t.Token("22", "aber", "aber", "_", "ADV", "_", "_",
                    "_", "14", "_", "MO", "_", "_", "_", "_"),
            sentence.t.Token("23", ":", ":", "_", "$.", "_", "_",
                    "_", "5", "_", "PUNCT", "_", "_", "_", "_"))


## (queue Sentence) -> (unionof String False)
def lfg_parse(candidates):
    while True:
        s = candidates.get()
        if s is Over:
            return 'False'
        else:
            for _, parsed in config.LFG_PARSER.parse([s], None, None):
                if parsed:
                    return sentence.s2string(s)


## http://stackoverflow.com/a/13821695
def timeout(func, args=(), kwargs={}, timeout_duration=1, default=None):
    import signal

    class TimeoutError(Exception):
        pass

    def handler(signum, frame):
        raise TimeoutError()

    # set the timeout handler
    signal.signal(signal.SIGALRM, handler)
    signal.alarm(timeout_duration)
    try:
        result = func(*args, **kwargs)
    except TimeoutError as exc:
        result = default
    finally:
        signal.alarm(0)

    return result


# String Int -> (union String, 'timeout reached', False)
def simplify_and_parse(s, t):

    def __simplify_and_parse(s):
        candidates = Queue()

        for simpler_s in simplifier.simplify(string2sentence(s), 'OneTokenShorterRecursiveBFSearch', 'DoNotSort', 10):
            candidates.put(simpler_s)
        candidates.put(Over)

        return lfg_parse(candidates)

    return timeout(__simplify_and_parse, (s,), {}, t, 'timeout was reached')

if __name__ == '__main__':
    print(simplify_and_parse('Ich sehe ihm.', 10))  # -> "Ich sehe ."
    print(simplify_and_parse(' a b c d f .', 10))    # -> False
    print(simplify_and_parse('`` Ich glaube kaum , daß mit seinem , naja , etwas undiplomatischen Stil im Weißen Haus dem Land ein Gefallen getan wäre .', 1))  # -> Timeout
