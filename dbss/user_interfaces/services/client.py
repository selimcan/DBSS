import zerorpc
import sys

from dbss.core import sentence

PORT = '4244'

c = zerorpc.Client()
c.bind("tcp://*:" + PORT)

for s in sentence.read_sentences(sys.argv[1]):
    c.simplify(s)
