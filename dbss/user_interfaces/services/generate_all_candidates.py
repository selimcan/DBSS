import os
import zerorpc
import sys

from dbss.core import sentence, simplifier

## Listens on port PORT for calls to the 'simplify' function.
## See also client.py file.
## USAGE: python generate_all_candidates.py <ip_adress_to_run_on>
##        e.g. python generate_all_candidates.py 141.58.127.109

PORT = '4244'
ALL_POSSIBLE_CANDIDATES_DIR = 'AllCandidates/'
GENERATION_MODE = 'OneSubtreeShorterRecursive'
SORTING_MODE = None
MAX_NUMBER_OF_CANDIDATES = 1000000  ## in the paper, ~960000 was reported as the highest
                                    ## number of candidates. It seems here we get more.
                                    ## TODO: rerun sentences which gave one million
                                    ## candidates with a higher limit to see what was
                                    ## the real number of them.

                                    
class SentenceSimplifier(object):

    ## Sentence -> Void
    def simplify(self, s):
        """For a given sentence, generate all candidates obtained by deleting
        all possible combinations of removable subtrees and write them to a file
        named <sentence_id.conll09> in ALL_POSSIBLE_CANDIDATES_DIR.
        """
        s = [Token(*t) for t in s]
        output_file = ALL_POSSIBLE_CANDIDATES_DIR + get_id(s) + CONLL_EXTENSION
        print(output_file)
        if not os.path.exists(os.path.dirname(output_file)):
            os.makedirs(os.path.dirname(output_file))
        with open(output_file, 'w') as outf:
            for simpler_candidate in simplifier.simplify(s,
                                                         GENERATION_MODE,
                                                         SORTING_MODE,
                                                         MAX_NUMBER_OF_CANDIDATES):
                outf.write(s2conll(simpler_candidate) + '\n')


s = zerorpc.Server(SentenceSimplifier())
s.connect("tcp://" + sys.argv[1] + ":" + PORT)
s.run()
