#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import json

from dbss.core import token, sentence
from dbss.plugins.ranker.mlranker import mlranker

## INPUT: a tsv file of the form
## X\tY\tZ
## where X is the id of the original sentence,
##       Y is the number of simplified candidate for that sentence, and
##       Z is a space-separated list of deleted *token* id's.
## E.g.:
## 2\t1\t5 6
##
## ASSUME: id field of the conll09 file  with original sentences (ORIGINAL_SENTS_CONLL09)
##         is of the form: number_number, where the first number is the sentence id,
##         the second number is the token id proper.

ORIGINAL_SENTS_CONLL09 = 'data/experiments/input/tiger_train_easyPunct_xleFail.conll09'

with open(ORIGINAL_SENTS_CONLL09) as osf:
    origs = {sentence.get_id(s): s for s in sentence.read_sentences(osf)}

RANKER = mlranker.MLRanker()

def provide_input():
    for line in sys.stdin:
        if line.strip():
            x, y, z = line.split('\t')
            original = origs[x]
            to_delete = set(z.split())
            simplified = [t for t in original if token.get_id(t.id) not in to_delete]
            yield original, simplified, []

for orig, simplified, score in RANKER.score(provide_input()):
    print(json.dumps({sentence.s2string(orig): (sentence.s2string(simplified), score)},
                     ensure_ascii=False))

## OUTPUT:
## {'Original sentence here': ['Simplified Candidate 1', 0.1234]}
## {'Same original sentence here': ['Simplified Candidate 2', 0.5678]}
