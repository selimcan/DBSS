#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from collections import deque
import heapq

from dbss import config
from dbss.core import token, sentence, parsability_table

## A sentence simplifier.
## UNIT TESTS: python3 -m pytest simplifier.py
##             (py.test has to be installed)


## =================
## Constants:


PARSABILITY = {}


## ==========
## Functions:


## Function Sentence Boolean Function -> (generator Sentence)
def simpler_candidates(simplifier, s, recursive, local_sorter):
    """Using the simplifier function, generate simplified versions of s in order according to the sorter;
    if recursive is True, then simplified versions of its first simplified version etc. until there is
    nothing to reduce.
    """
    q_of_generators = deque([simplifier(s, s)])
    q_of_original_sentences = deque([s])
    seen = set()
    while q_of_generators:
        one_shorter_generator = q_of_generators.popleft()
        s_before_reduction = q_of_original_sentences.popleft()
        for simpler_s in local_sorter(list(one_shorter_generator),
                                      s_before_reduction):
            if sentence.s2string(simpler_s) not in seen:
                yield simpler_s
                seen.add(sentence.s2string(simpler_s))
                if recursive:
                    q_of_generators.append(simplifier(simpler_s, s))
                    q_of_original_sentences.append(simpler_s)

def test_simpler_candidates():
    assert list(simpler_candidates(one_token_shorter, sentence.S_0, True, do_not_sort)) == []
    assert list(map(sentence.s2string, simpler_candidates(one_token_shorter, sentence.S_1, True, do_not_sort))) ==\
           ["sehe .", "Ich .", "Ich sehe",
            ".", "sehe", "Ich",
            ""]
    assert list(map(sentence.s2string, simpler_candidates(one_token_shorter, sentence.S_1, False, do_not_sort))) ==\
           ["sehe .", "Ich .", "Ich sehe"]


## Sentence -> Sentence
def remove_all(s):
    """Remove all deletable subtrees."""
    orig = s
    for t in orig:
        if head_of_deletable_subtree(t, orig):
           s = remove_subtree_starting_with(t.id, s)
    return s

def test_remove_all():
    sentence.s2string(remove_all(sentence.S_5)) == 'Ein Sprecher hat verkündigt , man werde Aussage analysieren'


## ------------------
## sorting functions:


## (generator Sentence) Sentence -> (generator Sentence)
def do_not_sort(candidates, original_s):
    """Return candidates back in the same order."""
    for simpler_s in candidates:
        yield simpler_s

def test_do_not_sort():
    assert list(do_not_sort([], sentence.S_4)) == []
    assert list(do_not_sort(one_token_shorter(sentence.S_4, sentence.S_4), sentence.S_4)) == list(one_token_shorter(sentence.S_4, sentence.S_4))


## (generator Sentence) Sentence -> (generator Sentence)
def sort_by_parsability(candidates, original_s):
    """Sort candidates so that candidates with least parsable elements of original_s removed
    appear first.

    If two or more deleted elements have the same parsability score, a candidate where deleted
    element is closer to the beginning of the original_s is taken.

    ASSUME: - originals_s is not empty
            - a ParsabilityTable named PARSABILITY is defined globally
            - parsability score for an unknown 1-gram is 1.0
            - parsability score for an unknown n-gram with n > 1 is the minimum
                  of parsabilities of [n-1]-grams it consists of
    """

    ## String -> Float
    def parsability(ngram):
        if ngram not in PARSABILITY:
            if len(ngram.split()) == 1:
                return 1.0
            else:
                return min(map(parsability, [' '.join(sub_ngram) for sub_ngram in
                                              list(find_sub_ngrams(ngram.split(), len(ngram.split()) - 1))]))
        else:
            return PARSABILITY[ngram]

    ## List Integer -> Zip
    def find_sub_ngrams(ngram, n):
        return zip(*[ngram[i:] for i in range(n)])

    for candidate in sorted(candidates, key=lambda c: parsability(sentence.s2string(diff(original_s, c)))):
        yield candidate

def test_sort_by_parsability():
    global PARSABILITY
    PARSABILITY = {'a': 0.3, 'b': 0.2, 'c': 0.1, '.': 0.0}
    assert list(sort_by_parsability([], sentence.S_4)) == []

    assert list(map(sentence.s2string, sort_by_parsability(list(one_token_shorter(sentence.S_4, sentence.S_4)), sentence.S_4))) ==\
           ["a b c", 'a b .', 'a c .', "b c ."]

    PARSABILITY = {'a': 0.3, 'c': 0.1, '.': 0.0}
    assert list(map(sentence.s2string, sort_by_parsability(list(one_token_shorter(sentence.S_4, sentence.S_4)), sentence.S_4))) ==\
           ["a b c", 'a b .', "b c .", 'a c .']

    PARSABILITY = {'a': 0.3, 'c': 0.1}
    assert list(map(sentence.s2string, sort_by_parsability(list(one_token_shorter(sentence.S_4, sentence.S_4)), sentence.S_4))) ==\
           ["a b .", 'b c .', "a c .", 'a b c']

    PARSABILITY = {'japanischen': 0.553,
                   'des japanischen Außenministerium': 0.034,
                   'daraufhin': 0.754,
                   'Jelzins': 0.437,
                   'vorsichtig': 0.927,
                   'bevor man sie kommentiere': 0.350,
                   'aber': 0.598}
    assert list(map(sentence.s2string, sort_by_parsability(list(one_subtree_shorter(sentence.S_5, sentence.S_5)), sentence.S_5))) ==\
           ["Ein Sprecher verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , , aber :",
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",
            "Ein Sprecher des Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , :",
            "Ein Sprecher des japanischen Außenministerium verkündete , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` analysieren '' , bevor man sie kommentiere , aber :"]

    PARSABILITY = {'japanischen': 0.553,
                   'daraufhin': 0.754,
                   'Jelzins': 0.437,
                   'vorsichtig': 0.927,
                   'bevor man sie kommentiere': 0.350,
                   'aber': 0.598,
                   'japanischen Außenministerium': 0.509}
    assert list(map(sentence.s2string, sort_by_parsability(list(one_subtree_shorter(sentence.S_5, sentence.S_5)), sentence.S_5))) ==\
           ["Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , , aber :",
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",
            "Ein Sprecher verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",
            "Ein Sprecher des Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , :",
            "Ein Sprecher des japanischen Außenministerium verkündete , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` analysieren '' , bevor man sie kommentiere , aber :"]


## (generator Sentence) Sentence -> (generator Sentence)
def shortest_first(candidates, original_s):
    """Sort candidates so that candidates with the least number of tokens appear first.
    If two or more sentences are of the same length, keep the order of the input.
    """
    for candidate in sorted(candidates, key=lambda c: len(c)):
        yield candidate

def test_shortest_first():
    assert list(map(sentence.s2string, shortest_first(list(one_subtree_shorter(sentence.S_5, sentence.S_5)), sentence.S_5))) ==\
           ["Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , , aber :",  # 20
            "Ein Sprecher verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",  # 21
            "Ein Sprecher des Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",  # 23
            "Ein Sprecher des japanischen Außenministerium verkündete , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",  # 23
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",  # 23
             "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
             " `` analysieren '' , bevor man sie kommentiere , aber :",
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , :"]  # 23


## (generator Sentence) Sentence -> (generator Sentence)
def longest_first(candidates, original_s):
    """Sort candidates so that candidates with the highest number of tokens appear first.
    If two or more sentences are of the same length, keep the order of the input.
    """
    for candidate in sorted(candidates, key=lambda c: len(c), reverse=True):
        yield candidate

def test_longest_first():
    assert list(map(sentence.s2string, longest_first(list(one_subtree_shorter(sentence.S_5, sentence.S_5)), sentence.S_5))) ==\
           ["Ein Sprecher des Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",  # 23
            "Ein Sprecher des japanischen Außenministerium verkündete , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",  # 23
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",  # 23
             "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
             " `` analysieren '' , bevor man sie kommentiere , aber :",  # 23
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , :",  # 23
            "Ein Sprecher verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",  # 21
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , , aber :"]  # 20


## Sentence Sentence -> Sentence  # (listof Token), if you wish
def diff(s1, s2):
    """"Return a list of tokens which have been deleted from s1
    when producing s2."""
    return [t for t in s1 if t not in s2]

# TODO
# def test_diff():


## ----------------------------
## Simpler sentence generators:


## Sentence Sentence -> (generator Sentence)
def one_token_shorter(s, original_s):
    """Generate sentences one token shorter than s."""
    if not s:
        return
    else:
        for n in range(len(s)):
            yield remove_nth_t(n, s)

def test_one_token_shorter():
    assert list(one_token_shorter(sentence.S_0, sentence.S_0)) == []
    assert list(map(sentence.s2string, one_token_shorter(sentence.S_3, sentence.S_3))) ==\
           ["sehe ihm .", "Ich ihm .", "Ich sehe .", "Ich sehe ihm"]


## Sentence Sentence -> (generator Sentence)
def one_subtree_shorter(s, original_s):
    """Generate sentences one deletable subtree shorter than s."""
    if not s:
        return
    else:
        for t in s:
            if head_of_deletable_subtree(t, original_s):
                yield remove_subtree_starting_with(t.id, s)

def test_one_subtree_shorter():
    assert list(one_subtree_shorter(sentence.S_0, sentence.S_0)) == []
    assert list(one_subtree_shorter(sentence.S_1, sentence.S_1)) == []
    assert list(map(sentence.s2string, one_subtree_shorter(sentence.S_5, sentence.S_5))) ==\
    ["Ein Sprecher des Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
     " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",
     "Ein Sprecher verkündete daraufhin , man werde Jelzins Aussage"
     " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",
     "Ein Sprecher des japanischen Außenministerium verkündete , man werde Jelzins Aussage"
     " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",
     "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Aussage"
     " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",
     "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
     " `` analysieren '' , bevor man sie kommentiere , aber :",
     "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
     " `` vorsichtig analysieren '' , , aber :",
     "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
     " `` vorsichtig analysieren '' , bevor man sie kommentiere , :"]
    assert list(map(sentence.s2string, one_subtree_shorter([token.Token("880_1", "Ein", "ein", "_", "ART", "_",
                                                         "case=nom|number=sg|gender=masc",
                                                         "_", "2", "_", "NK", "_", "_", "_", "_"),
                                                   token.Token("880_2", "Sprecher", "Sprecher", "_", "NN", "_",
                                                         "case=nom|number=sg|gender=masc",
                                                         "_", "6", "_", "SB", "_", "_", "_", "_"),
                                                   token.Token("880_6", "verkündete", "verkünden", "_", "VVFIN", "_",
                                                         "number=sg|person=3|tense=past|mood=ind",
                                                         "_", "0", "_", "--", "_", "_", "_", "_"),
                                                   token.Token("880_23", "aber", "aber", "_", "ADV", "_", "_",
                                                         "_", "15", "_", "MO", "_", "_", "_", "_")],
                                                  sentence.S_5))) == ['Ein Sprecher verkündete']


## -----------------------------------
## Actions for simplifying a sentence:


## Integer Sentence -> Sentence
def remove_nth_t(n, s):
    """Return a new sentence with n-th token removed.
    ASSUME: - s is not empty
            - we count from 0
            - n is never out of range
    """
    return tuple(s[:n] + s[n + 1:])

def test_remove_nth_t():
    assert sentence.s2string(remove_nth_t(0, sentence.S_2)) == "sehe ihn ."
    assert sentence.s2string(remove_nth_t(2, sentence.S_2)) == "Ich sehe ."
    assert sentence.s2string(remove_nth_t(3, sentence.S_2)) == "Ich sehe ihn"


## String Sentence -> Sentence
def remove_t_with_id(id, s):
    """Return a new sentence with token having id 'id' removed.
    ASSUME: - sentence actually contains the token with given id and id is unique
    """
    return tuple(t for t in s if t.id != id)

def test_remove_t_with_id():
    assert remove_t_with_id("1_1", sentence.S_1) == sentence.S_1[1:]
    assert remove_t_with_id("1_2", sentence.S_1) == (sentence.S_1[0], sentence.S_1[2])
    assert remove_t_with_id("1_3", sentence.S_1) == sentence.S_1[:-1]


## String Sentence -> Sentence
def remove_subtree_starting_with(id, s):
    """Return a new sentence with token with given id and all its children removed."""
    shorter_s = s
    to_delete = [id]
    for id in to_delete:
        to_delete.extend(children(id, s))
        shorter_s = remove_t_with_id(id, shorter_s)
    return shorter_s

def test_remove_subtree_starting_with():
    assert sentence.s2string(remove_subtree_starting_with("880_5", sentence.S_5)) ==\
           "Ein Sprecher verkündete daraufhin , man werde Jelzins Aussage `` vorsichtig analysieren '' ," \
           " bevor man sie kommentiere , aber :"


## String Sentence -> (listof String)
def children(id, s):
    """Return id's of children of token with given id."""
    return [t.id for t in s if t.head == token.get_id(id)]

def test_children():
    assert children("880_1", sentence.S_5) == []
    assert children("880_12", sentence.S_5) == ["880_11"]
    assert children("880_5", sentence.S_5) == ["880_3", "880_4"]


## Token Sentence -> Boolean
def head_of_deletable_subtree(t, s):
    """Produce True if token is the head of a deletable subtree of the sentence."""
    if t.deprel in config.DELETABLE_SUBTREES:
        if t.deprel == 'NK':
            if t.pos in ['ADJA', 'ADJD', 'ADV', 'KOUS']:
                return True
            elif t.pos == 'NN' and s[int(t.head) - 1].pos == 'NN':  # TODO: consider adding a dummy ROOT at index 0
                                                                    # to each sentence and storing t.head as integer
                return True
            else:
                return False
        else:
            return True
    else:
        return False

def test_head_of_deletable_subtree():
    # NK requires additional tests, others listed in config.DELETABLE_SUBTREES do not
    # (according to getOneSubtreeShorterCandidates.pl)
    assert head_of_deletable_subtree(token.Token("880_2", "Sprecher", "Sprecher", "_", "NN", "_",
                                           "case=nom|number=sg|gender=masc", "_", "6", "_", "SB", "_", "_", "_", "_"),
                                     sentence.S_5) ==\
           False
    assert head_of_deletable_subtree(token.Token("880_4", "japanischen", "japanisch", "_", "ADJA", "_",
                                           "case=gen|number=sg|gender=neut|degree=pos", "_", "5", "_", "NK", "_",
                                           "_", "_", "_"),
                                     sentence.S_5) ==\
           True
    assert head_of_deletable_subtree(token.Token("22_7", "Präsident", "Präsident", "_", "NN", "_",
                                           "case=nom|number=sg|gender=masc", "_", "6", "_", "NK", "_", "_", "_", "_"),
                                     sentence.S_6) ==\
           True
    assert head_of_deletable_subtree(token.Token("880_3", "des", "der", "_", "ART", "_", "case=gen|number=sg|gender=neut",
                                           "_", "5", "_", "NK", "_", "_", "_", "_"),
                                     sentence.S_5) ==\
           False
    assert head_of_deletable_subtree(token.Token("880_5", "Außenministerium", "Außenministerium", "_", "NN", "_",
                                           "case=gen|number=sg|gender=neut", "_", "2", "_", "AG", "_", "_", "_", "_"),
                                     sentence.S_5) ==\
           True


## Sentence -> Sentence
def remove_punct(s):
    """Remove punctuation symbols from the sentence.

    ASSUME: - part-of-speech tag of punctuation symbols starts with a $ sign
              and there are no other part-of-speech tags starting with a $ sign.
    """
    return tuple(t for t in s if t.pos[0] != '$')

def test_remove_punct():
    assert remove_punct(sentence.S_0) == ()
    assert remove_punct(sentence.S_1) == sentence.S_1
    assert sentence.s2string(remove_punct(sentence.S_5)) == "Ein Sprecher des japanischen Außenministerium verkündete daraufhin " \
                                          "man werde Jelzins Aussage vorsichtig analysieren " \
                                          "bevor man sie kommentiere aber"


## ----------------------------------
## Limiting the number of candidates:


## Integer (generator Sentence) -> (generator Sentence)
def take_n(n, generator):
    """Take at most n items from the generator."""
    i = 0
    while i < n:
        try:
            yield next(generator)
            i += 1
        except StopIteration:
            raise StopIteration

def test_take_n():
    assert list(take_n(2, iter([1, 2, 3, 4, 5]))) == [1, 2]
    assert list(take_n(6, iter([1, 2, 3, 4, 5]))) == [1, 2, 3, 4, 5]


## Integer (generator Sentence) Function -> (generator Sentence)
def take_n_best(n, generator, key_for_sorting):
    """Take n best sentences from a sentence generator (the lower the value
    key_for_sorting function gives for a sentence, the better is the sentence).
    """
    return iter(heapq.nsmallest(n, generator, key=key_for_sorting))

def test_take_n_best():
    assert list(map(sentence.s2string, take_n_best(1, one_subtree_shorter(sentence.S_5, sentence.S_5), lambda s: len(s)))) ==\
           ["Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , , aber :"]  # 20
    assert list(map(sentence.s2string, take_n_best(1, one_subtree_shorter(sentence.S_5, sentence.S_5), lambda s: -len(s)))) ==\
           ["Ein Sprecher des Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :"]  # 23
    assert list(map(sentence.s2string, take_n_best(2, one_subtree_shorter(sentence.S_5, sentence.S_5), lambda s: len(s)))) ==\
           ["Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , , aber :",  # 20
            "Ein Sprecher verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :"]  # 21
    assert list(map(sentence.s2string, take_n_best(15, one_subtree_shorter(sentence.S_5, sentence.S_5), lambda s: -len(s)))) ==\
           ["Ein Sprecher des Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",  # 23
            "Ein Sprecher des japanischen Außenministerium verkündete , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",  # 23
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",  # 23
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` analysieren '' , bevor man sie kommentiere , aber :",  # 23
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , :",  # 23
            "Ein Sprecher verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",  # 21
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , , aber :"]  # 20


## Integer (generator Sentence) -> (generator Sentence)
def take_n_shortest(n, generator):
    """Take n shortest sentences (in terms of the number of tokens)
    from a sentence generator.
    """
    return take_n_best(n, generator, lambda s: len(s))

def test_take_n_shortest():
    assert list(map(sentence.s2string, take_n_shortest(1, one_subtree_shorter(sentence.S_5, sentence.S_5)))) ==\
           ["Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , , aber :"]  # 20
    assert list(map(sentence.s2string, take_n_shortest(2, one_subtree_shorter(sentence.S_5, sentence.S_5)))) ==\
           ["Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , , aber :",  # 20
            "Ein Sprecher verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :"]  # 21
    assert list(map(sentence.s2string, take_n_shortest(15, one_subtree_shorter(sentence.S_5, sentence.S_5)))) ==\
           ["Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , , aber :",  # 20
            "Ein Sprecher verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",  # 21
            "Ein Sprecher des Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",  # 23
            "Ein Sprecher des japanischen Außenministerium verkündete , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",  # 23
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",  # 23
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` analysieren '' , bevor man sie kommentiere , aber :",  # 23
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , :"]  # 23


## Integer (generator Sentence) -> (generator Sentence)
def take_n_longest(n, generator):
    """Take n longest sentences (in terms of the number of tokens)
    from a sentence generator.
    """
    return take_n_best(n, generator, lambda s: -len(s))

def test_take_n_longest():
    assert list(map(sentence.s2string, take_n_longest(1, one_subtree_shorter(sentence.S_5, sentence.S_5)))) ==\
           ["Ein Sprecher des Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :"]  # 23
    assert list(map(sentence.s2string, take_n_longest(2, one_subtree_shorter(sentence.S_5, sentence.S_5)))) ==\
           ["Ein Sprecher des Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",  # 23
            "Ein Sprecher des japanischen Außenministerium verkündete , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :"]  # 23
    assert list(map(sentence.s2string, take_n_longest(15, one_subtree_shorter(sentence.S_5, sentence.S_5)))) ==\
           ["Ein Sprecher des Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",  # 23
            "Ein Sprecher des japanischen Außenministerium verkündete , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",  # 23
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",  # 23
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` analysieren '' , bevor man sie kommentiere , aber :",  # 23
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , :",  # 23
            "Ein Sprecher verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",  # 21
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , , aber :"]  # 20


## Integer (generator Sentence) -> (generator Sentence)
def take_n_plus_first_wo_punct(n, generator):
    """Take at most n sentences from the generator, but as 0th sentence yield the first
    sentence without punctuation.
    """
    try:
        first = next(generator)
        yield remove_punct(first)
        yield first
    except StopIteration:
        raise StopIteration
    i = 1
    while i < n:
        try:
            yield next(generator)
            i += 1
        except StopIteration:
            raise StopIteration

def test_take_n_plus_first_wo_punct():
    assert list(map(sentence.s2string,
                    take_n_plus_first_wo_punct(10,
                                               shortest_first(simpler_candidates(one_subtree_shorter,
                                                                                 sentence.S_5,
                                                                                 True,
                                                                                 do_not_sort),
                                                              sentence.S_5)))) == \
           ["Ein Sprecher verkündete man werde Aussage analysieren",
            "Ein Sprecher verkündete , man werde Aussage `` analysieren '' , , :",
            "Ein Sprecher verkündete , man werde Aussage `` analysieren '' , , aber :",
            "Ein Sprecher verkündete , man werde Aussage `` vorsichtig analysieren '' , , :",
            "Ein Sprecher verkündete , man werde Jelzins Aussage `` analysieren '' , , :",
            "Ein Sprecher verkündete daraufhin , man werde Aussage `` analysieren '' , , :",
            "Ein Sprecher verkündete , man werde Aussage `` vorsichtig analysieren '' , , aber :",
            "Ein Sprecher verkündete , man werde Jelzins Aussage `` analysieren '' , , aber :",
            "Ein Sprecher verkündete , man werde Jelzins Aussage `` vorsichtig analysieren '' , , :",
            "Ein Sprecher verkündete daraufhin , man werde Aussage `` analysieren '' , , aber :",
            "Ein Sprecher verkündete daraufhin , man werde Aussage `` vorsichtig analysieren '' , , :"]
    assert list(map(sentence.s2string,
                    take_n_plus_first_wo_punct(3,
                                               shortest_first(simpler_candidates(one_subtree_shorter,
                                                                                 sentence.S_5,
                                                                                 False,
                                                                                 do_not_sort),
                                                              sentence.S_5)))) == \
           ["Ein Sprecher des japanischen Außenministerium verkündete daraufhin man werde Jelzins Aussage"
            " vorsichtig analysieren aber",
            "Ein Sprecher des japanischen Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , , aber :",
            "Ein Sprecher verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :",
            "Ein Sprecher des Außenministerium verkündete daraufhin , man werde Jelzins Aussage"
            " `` vorsichtig analysieren '' , bevor man sie kommentiere , aber :"]


## Integer (generator Sentence) -> (generator Sentence)
def take_n_shortest_plus_shortest_wo_punct(n, generator):
    """Take at most n shortest sentences from the generator, but as 0th sentence yield the shortest
    sentence without punctuation.
    """
    return take_n_plus_first_wo_punct(n, take_n_shortest(n, generator))

def test_take_n_shortest_plus_shortest_wo_punct():
    assert list(map(sentence.s2string,
                    take_n_shortest_plus_shortest_wo_punct(10,
                                                           simpler_candidates(one_subtree_shorter,
                                                                              sentence.S_5,
                                                                              True,
                                                                              do_not_sort)))) ==\
           ["Ein Sprecher verkündete man werde Aussage analysieren",
            "Ein Sprecher verkündete , man werde Aussage `` analysieren '' , , :",
            "Ein Sprecher verkündete , man werde Aussage `` analysieren '' , , aber :",
            "Ein Sprecher verkündete , man werde Aussage `` vorsichtig analysieren '' , , :",
            "Ein Sprecher verkündete , man werde Jelzins Aussage `` analysieren '' , , :",
            "Ein Sprecher verkündete daraufhin , man werde Aussage `` analysieren '' , , :",
            "Ein Sprecher verkündete , man werde Aussage `` vorsichtig analysieren '' , , aber :",
            "Ein Sprecher verkündete , man werde Jelzins Aussage `` analysieren '' , , aber :",
            "Ein Sprecher verkündete , man werde Jelzins Aussage `` vorsichtig analysieren '' , , :",
            "Ein Sprecher verkündete daraufhin , man werde Aussage `` analysieren '' , , aber :",
            "Ein Sprecher verkündete daraufhin , man werde Aussage `` vorsichtig analysieren '' , , :"]
    assert list(map(sentence.s2string,
                    take_n_shortest_plus_shortest_wo_punct(1,
                                                           simpler_candidates(one_subtree_shorter,
                                                                              sentence.S_5,
                                                                              True,
                                                                              do_not_sort)))) ==\
           ["Ein Sprecher verkündete man werde Aussage analysieren",
            "Ein Sprecher verkündete , man werde Aussage `` analysieren '' , , :"]


## Integer (generator Sentence) -> (generator Sentence)
def take_n_longest_plus_longest_wo_punct(n, generator):
    """Take at most n longest sentences from the generator, but as 0th sentence yield the longest
    sentence with punctuation removed.
    """
    return take_n_plus_first_wo_punct(n, take_n_longest(n, generator))

# TODO def test_take_n_longest_plus_longest_wo_punct():


## =================
## Runner/Interface:


MODE2GENERATOR_RECURSIVE_LOCAL_GLOBAL_TAKE = \
    {'OneTokenShorterRecursiveBFSearch': (one_token_shorter, True, do_not_sort, do_not_sort,
                                          take_n, config.DEFAULT_MAX_NUMBER_OF_CANDIDATES),
     'OneTokenShorterNonRecursive': (one_token_shorter, False, do_not_sort, do_not_sort,
                                     take_n, config.DEFAULT_MAX_NUMBER_OF_CANDIDATES),
     'OneSubtreeShorterRecursive': (one_subtree_shorter, True, do_not_sort, do_not_sort,
                                    take_n, config.DEFAULT_MAX_NUMBER_OF_CANDIDATES),
     'OneSubtreeShorterRecursive+FirstWithoutPunctuation': (one_subtree_shorter, True, do_not_sort, do_not_sort,
                                                            take_n_plus_first_wo_punct,
                                                            config.DEFAULT_MAX_NUMBER_OF_CANDIDATES),
     'OneSubtreeShorterNonRecursive': (one_subtree_shorter, False, do_not_sort, do_not_sort,
                                       take_n, config.DEFAULT_MAX_NUMBER_OF_CANDIDATES),
     '10Shortest+ShortestWithoutPunctuation': (one_subtree_shorter, True, do_not_sort, do_not_sort,
                                               take_n_shortest_plus_shortest_wo_punct,
                                               10),
     '10Longest+LongestWithoutPunctuation': (one_subtree_shorter, True, do_not_sort, longest_first,
                                             take_n_longest_plus_longest_wo_punct,
                                             10),
     '10Shortest': (one_subtree_shorter, True, do_not_sort, do_not_sort,
                    take_n_shortest, 10)}


SORTING_MODE2LOCAL_GLOBAL = {'GreedyByParsability': (sort_by_parsability, do_not_sort),
                             'GreedyShortestFirst': (shortest_first, do_not_sort),  # TODO
                             'OptimalByParsability': (do_not_sort, sort_by_parsability),  # TODO
                             'OptimalShortestFirst': (do_not_sort, shortest_first),
                             'OptimalLongestFirst': (do_not_sort, longest_first),
                             'DoNotSort': (do_not_sort, do_not_sort)}


## Sentence String String Integer -> (generator Sentence)
def simplify(s, simplification_mode, sorting_mode, max_nbr_of_candidates):
    """Generate simpler versions of s according to given simplification
    and sorting modes.
    """
    simplifier, recursive, local_sorter, global_sorter, take, nbr_of_candidates =\
        MODE2GENERATOR_RECURSIVE_LOCAL_GLOBAL_TAKE[simplification_mode]
    if sorting_mode:
        local_sorter, global_sorter = SORTING_MODE2LOCAL_GLOBAL[sorting_mode]
    if nbr_of_candidates < max_nbr_of_candidates:
        max_nbr_of_candidates = nbr_of_candidates
    return take(max_nbr_of_candidates,
                global_sorter(simpler_candidates(simplifier,
                                                 s,
                                                 recursive,
                                                 local_sorter),
                              s))

def test_simplify():
    assert list(simplify(sentence.S_0, 'OneTokenShorterRecursiveBFSearch', 'DoNotSort',
                         config.DEFAULT_MAX_NUMBER_OF_CANDIDATES)) == []
    assert list(map(sentence.s2string, simplify(sentence.S_1, 'OneTokenShorterRecursiveBFSearch', 'DoNotSort',
                                       config.DEFAULT_MAX_NUMBER_OF_CANDIDATES))) ==\
           ["sehe .", "Ich .", "Ich sehe",
            ".", "sehe", "Ich",
            ""]
    assert list(map(sentence.s2string, simplify(sentence.S_1, 'OneTokenShorterNonRecursive', 'DoNotSort',
                                       config.DEFAULT_MAX_NUMBER_OF_CANDIDATES))) ==\
           ["sehe .", "Ich .", "Ich sehe"]
