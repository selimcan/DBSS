"""
Some data type and constant definitions common for all other modules.
UNIT TESTS: python3 -m pytest dbss/core/token.py
            (py.test has to be installed)
"""

from collections import namedtuple


## =================
## Data definitions:


Token = namedtuple("Token", ["id", "form", "lemma", "plemma", "pos", "ppos",
                             "feat", "pfeat", "head", "phead", "deprel", "pdeprel",
                             "fillpred", "pred", "apreds"])
## Token is Token(String, String, String, String, String
##                String, String, String, String, String
##                String, String, String, String, String)
## interp. one word of a sentence with information provided in the CoNLL-2009 format

T_1 = Token("4_6", "hat", "haben", "_", "VAFIN", "_", "number=sg|person=3|tense=pres|mood=ind",
            "_", "0", "_", "--", "_", "_", "_", "_")

T_2 = Token("7_3", "Fähigkeiten", "Fähigkeit", "_", "NN", "_", "case=nom|number=pl|gender=fem",
            "_", "_", "_", "_", "_", "_", "_", "_")

T_3 = Token("7_3", "Fähigkeiten", "_", "_", "_", "_", "_", "_", "_", "_", "_", "_", "_", "_", "_")

"""
def fn_for_token(t):
    ... t.id         # String
        t.form       # String
        t.lemma      # String
        t.plemma     # String
        t.pos        # String
        t.ppos       # String
        t.feat       # String
        t.pfeat      # String
        t.head       # String
        t.phead      # String
        t.deprel     # String
        t.pdeprel    # String
        t.fillpred   # String
        t.fillpred   # String
        t.apreds     # String
"""
## Template rules:
##  - compound: 15 fields


## =================================================================
## Functions (to construct and operate on data types defined above):


## String -> Token
def read_token(line):
    """Parse a line of the file in conll09 format and return a Token."""
    return Token(*line.strip().split('\t'))

def test_read_token():
    assert read_token("3_1\tIch\t_\t_\t_\t_\t_\t_\t0\t_\t_\t_\t_\t_\t_\n") == \
        Token("3_1", "Ich", "_", "_", "_", "_", "_", "_", "0", "_", "_", "_", "_", "_", "_")
    assert read_token("  3_1\tIch\t_\t_\t_\t_\t_\t_\t0\t_\t_\t_\t_\t_\t_  \n") == \
        Token("3_1", "Ich", "_", "_", "_", "_", "_", "_", "0", "_", "_", "_", "_", "_", "_")


## String -> String
def get_id(complex_id):
    """Return token id.
    ASSUME: - 'id' field of tokens is of the form "number_number", where the second number is actual token id.
    """
    return complex_id.split('_')[1]

def test_get_id():
    assert get_id("4_6") == "6"
