#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import abc

"""
This module provides an abstract class (think of it as an Interface if you
are a Java programmer) with a method called 'parse'.

That method takes an iterable with sentences as input and generates
(Sentence, Boolean) tuples as output, where Boolean indicates whether
the sentence received a solution or not.

In the most basic scenario, 'receiving a solution' means getting a full
(i.e. non-fragmented) parse from the LFG parser, although further constraints like
checking for the Tiger-corpus compatibility of obtained parse trees are
possible.

Actual implementations of the class are in the 'plugins/lfg_parser' folder
(in classes which inherit from LfgParser).

Which implementation is used has to be specified manually
in the dbss.config.py file.

Note that it's in the plugins where we define what a 'successful parse'
is, persistent_parser.py doesn't care about it and relies on this
module's output, which in turn relies on plugins.

persistent_parser.py is supposed to work for any constituency parser for
which at least one such plugin implementing this abstract class is provided.
"""


class LfgParser(object):

    __metaclass__ = abc.ABCMeta

    ## (listof/generator Sentence) DirectoryName DirectoryName -> (generator (Sentence, Boolean))
    @staticmethod
    @abc.abstractmethod
    def parse(sentences, candidates_dir, lfg_parses_dir):
        """Parse sentences from the list, and generate (sentence, True) if the sentence received
        a desired solution from an Lfg parser and (sentence, False) if not.
        """
