#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re

## Some data type and constant definitions common for all other modules.
## UNIT TESTS: py.test-3 parsability_table.py OR python3 -m pytest parsability_table.py
##             (py.test version 3 has to be installed)


## =================
## Data definitions:


## ParsabilityTable is a Dictionary which maps Strings to Floats
## interp. parsability scores of n-grams as defined in [van Noord 2004].
##         The higher the parsability score, the more likely is the string to be parsed.

PT_1 = {"alpha": 0.300, "beta": 0.200, "gamma": 0.100, ".": 0.000}


## =================================================================
## Functions (to construct and operate on data types defined above):


## FileName -> ParsabilityTable
def read_parsabilities(f):
    """Return parsability scores from the file."""
    parsabilities = {}
    with open(f, 'r') as parsabilities_file:
        for line in parsabilities_file:
            try:
                parsability, count, ngram = parse_line_of_parsabilities_file(line)
                parsability = float(parsability)
                parsabilities[ngram] = parsability
            except TypeError:
                pass
    return parsabilities

def test_read_parsabilities(tmpdir):
    f = tmpdir.join('parsabilities.txt')
    f.write(" 0.300 1    a\n"
            "0.200 1    b\n"
            "\n"
            "     0.100     1        c   \n")
    assert read_parsabilities(str(f)) == {"a": 0.300, "b": 0.200, "c": 0.100}


## String -> String String String or None
def parse_line_of_parsabilities_file(line):
    """Return parsability score, frequency count and ngram.
    ASSUME: - lines of parsabilities file contain three space delimited values of the form:
                - a decimal with three digits after the period: 0.000
                - an integer: 123
                - a string of arbitrary length: des japanischen Außenministerium
    """
    match = re.search('^\s*(\d\.\d\d\d)\s+(\d+)\s+(.+)$', line)
    if match:
        return match.group(1), match.group(2), match.group(3).rstrip()
    else:
        return None

def test_parse_line_of_parsabilities_file():
    assert parse_line_of_parsabilities_file(" 0.000 1     ./") == ("0.000", "1", "./")
    assert parse_line_of_parsabilities_file("0.000 1  aber etwa  ") == ("0.000", "1", "aber etwa")
    assert parse_line_of_parsabilities_file(" aber etwa") == None
