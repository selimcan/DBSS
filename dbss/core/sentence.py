"""
Some data type and constant definitions common for all other modules.
UNIT TESTS: python3 -m pytest sentence.py
            (py.test version has to be installed)
"""

from io import StringIO

import dbss.core.token as t


## =================
## Data definitions:


## Sentence is (tupleof Token) of arbitrary size
## interp. a sentence to be parsed/simplified

S_0 = ()  # base case

S_1 = (t.Token("1_1", "Ich", "_", "_", "_", "_", "_", "_", "0", "_", "_", "_", "_", "_", "_"),   # ParGram parses this
       t.Token("1_2", "sehe", "_", "_", "_", "_", "_", "_", "0", "_", "_", "_", "_", "_", "_"),
       t.Token("1_3", ".", "_", "_", "_", "_", "_", "_", "0", "_", "_", "_", "_", "_", "_"))

S_2 = (t.Token("2_1", "Ich", "_", "_", "_", "_", "_", "_", "0", "_", "_", "_", "_", "_", "_"),   # ParGram parses this
       t.Token("2_2", "sehe", "_", "_", "_", "_", "_", "_", "0", "_", "_", "_", "_", "_", "_"),
       t.Token("2_3", "ihn", "_", "_", "_", "_", "_", "_", "0", "_", "_", "_", "_", "_", "_"),
       t.Token("2_4", ".", "_", "_", "_", "_", "_", "_", "0", "_", "_", "_", "_", "_", "_"))

S_3 = (t.Token("3_1", "Ich", "_", "_", "_", "_", "_", "_", "0", "_", "_", "_", "_", "_", "_"),   # ParGram will parse
       t.Token("3_2", "sehe", "_", "_", "_", "_", "_", "_", "0", "_", "_", "_", "_", "_", "_"),  # this after deleting
       t.Token("3_3", "ihm", "_", "_", "_", "_", "_", "_", "0", "_", "_", "_", "_", "_", "_"),   # 'ihm'
       t.Token("3_4", ".", "_", "_", "_", "_", "_", "_", "0", "_", "_", "_", "_", "_", "_"))

S_4 = (t.Token("4_1", "a", "_", "_", "_", "_", "_", "_", "0", "_", "_", "_", "_", "_", "_"),     # ParGram will not
       t.Token("4_2", "b", "_", "_", "_", "_", "_", "_", "0", "_", "_", "_", "_", "_", "_"),     # parse this even
       t.Token("4_3", "c", "_", "_", "_", "_", "_", "_", "0", "_", "_", "_", "_", "_", "_"),     # after simplification
       t.Token("4_3", ".", "_", "_", "_", "_", "_", "_", "0", "_", "_", "_", "_", "_", "_"))

S_5 = (t.Token("880_1", "Ein", "ein", "_", "ART", "_", "case=nom|number=sg|gender=masc",         # Token 5 causes
                "_", "2", "_", "NK", "_", "_", "_", "_"),                                        # trouble
       t.Token("880_2", "Sprecher", "Sprecher", "_", "NN", "_", "case=nom|number=sg|gender=masc",
                "_", "6", "_", "SB", "_", "_", "_", "_"),
       t.Token("880_3", "des", "der", "_", "ART", "_", "case=gen|number=sg|gender=neut",
                "_", "5", "_", "NK", "_", "_", "_", "_"),
       t.Token("880_4", "japanischen", "japanisch", "_", "ADJA", "_", "case=gen|number=sg|gender=neut|degree=pos",
                "_", "5", "_", "NK", "_", "_", "_", "_"),
       t.Token("880_5", "Außenministerium", "Außenministerium", "_", "NN", "_", "case=gen|number=sg|gender=neut",
                "_", "2", "_", "AG", "_", "_", "_", "_"),
       t.Token("880_6", "verkündete", "verkünden", "_", "VVFIN", "_", "number=sg|person=3|tense=past|mood=ind",
                "_", "0", "_", "--", "_", "_", "_", "_"),
       t.Token("880_7", "daraufhin", "daraufhin", "_", "PROAV", "_", "_",
                "_", "6", "_", "MO", "_", "_", "_", "_"),
       t.Token("880_8", ",", "--", "_", "$,", "_", "_", "_", "6", "_", "--", "_", "_", "_", "_"),
       t.Token("880_9", "man", "man", "_", "PIS", "_", "case=nom|number=sg|gender=*",
                "_", "10", "_", "SB", "_", "_", "_", "_"),
       t.Token("880_10", "werde", "werden", "_", "VAFIN", "_", "number=sg|person=3|tense=pres|mood=subj",
                "_", "6", "_", "OC", "_", "_", "_", "_"),
       t.Token("880_11", "Jelzins", "Jelzin", "_", "NE", "_", "case=gen|number=sg|gender=*",
                "_", "12", "_", "AG", "_", "_", "_", "_"),
       t.Token("880_12", "Aussage", "Aussage", "_", "NN", "_", "case=acc|number=sg|gender=fem",
                "_", "15", "_", "OA", "_", "_", "_", "_"),
       t.Token("880_13", "``", "--", "_", "$(", "_", "_", "_", "15", "_", "--", "_", "_", "_", "_"),
       t.Token("880_14", "vorsichtig", "vorsichtig", "_", "ADJD", "_", "degree=pos",
                "_", "15", "_", "MO", "_", "_", "_", "_"),
       t.Token("880_15", "analysieren", "analysieren", "_", "VVINF", "_", "_",
                "_", "10", "_", "OC", "_", "_", "_", "_"),
       t.Token("880_16", "''", "--", "_", "$(", "_", "_",
                "_", "15", "_", "--", "_", "_", "_", "_"),
       t.Token("880_17", ",", "--", "_", "$,", "_", "_",
                "_", "15", "_", "--", "_", "_", "_", "_"),
       t.Token("880_18", "bevor", "bevor", "_", "KOUS", "_", "_",
                "_", "21", "_", "CP", "_", "_", "_", "_"),
       t.Token("880_19", "man", "man", "_", "PIS", "_", "case=nom|number=sg|gender=*",
                "_", "21", "_", "SB", "_", "_", "_", "_"),
       t.Token("880_20", "sie", "sie", "_", "PPER", "_", "case=acc|number=sg|gender=fem|person=3",
                "_", "21", "_", "OA", "_", "_", "_", "_"),
       t.Token("880_21", "kommentiere", "kommentieren", "_", "VVFIN", "_", "number=sg|person=3|tense=pres|mood=subj",
                "_", "15", "_", "MO", "_", "_", "_", "_"),
       t.Token("880_22", ",", "--", "_", "$,", "_", "_",
                "_", "15", "_", "--", "_", "_", "_", "_"),
       t.Token("880_23", "aber", "aber", "_", "ADV", "_", "_",
                "_", "15", "_", "MO", "_", "_", "_", "_"),
       t.Token("880_24", ":", "--", "_", "$.", "_", "_",
                "_", "6", "_", "--", "_", "_", "_", "_"))

S_6 = (t.Token("22_1", "Die", "der", "_", "ART", "_", "case=nom|number=pl|gender=fem",
                "_", "2", "_", "NK", "_", "_", "_", "_"),
       t.Token("22_2", "Spekulationen", "Spekulation", "_", "NN", "_", "case=nom|number=pl|gender=fem",
                "_", "23", "_", "SB", "_", "_", "_", "_"),
       t.Token("22_3", "darüber", "darüber", "_", "PROAV", "_", "_",
                "_", "2", "_", "OP", "_", "_", "_", "_"),
       t.Token("22_4", ",", "--", "_", "$,", "_", "_",
                "_", "3", "_", "--", "_", "_", "_", "_"),
       t.Token("22_5", "welche", "welcher", "_", "PWAT", "_", "case=nom|number=sg|gender=fem",
                "_", "6", "_", "NK", "_", "_", "_", "_"),
       t.Token("22_6", "Art", "Art", "_", "NN", "_", "case=nom|number=sg|gender=fem",
                "_", "21", "_", "PD", "_", "_", "_", "_"),
       t.Token("22_7", "Präsident", "Präsident", "_", "NN", "_", "case=nom|number=sg|gender=masc",
                "_", "6", "_", "NK", "_", "_", "_", "_"),
       t.Token("22_8", "``", "--", "_", "$(", "_", "_",
                "_", "9", "_", "--", "_", "_", "_", "_"),
       t.Token("22_9", "Henry", "Henry", "_", "NE", "_", "case=nom|number=sg|gender=masc",
                "_", "21", "_", "SB", "_", "_", "_", "_")
       # ...
       )

S_7 = (t.Token("1_1", "Ich", "ich", "_", "PPER", "_", "nom|sg|*|1", "_", "2", "_", "SB", "_", "_", "_", "_"),
       t.Token("1_2", "lese", "lesen", "_", "VVFIN", "_", "sg|1|pres|ind", "_", "0", "_", "--", "_", "_", "_", "_"),
       t.Token("1_3", ".", "--", "_", "$.", "_", "_", "_", "2", "_", "--", "_", "_", "_", "_"))

"""
def fn_for_sent(s):
    if not s:
        ...
    else:
        for token in s:
            fn_for_token(token)
"""
## Template rules:
##  - one of: 2 cases
##  - atomic distinct: empty
##  - compound: (cons Token Sentence)
##  - reference: (first sent) is Token
##  - self-reference: (rest sent) is Sentence


## =================================================================
## Functions (to construct and operate on data types defined above):


## TextIOWrapper -> (generator Sentence)
def read_sentences(stream):
    """Return sentences in CoNLL09 format."""
    sent = []
    for line in stream:
        if not line.strip():  # empty line
            if sent:
                yield tuple(sent)
                sent = []
        else:
            sent.append(t.read_token(line))
    if sent:   # file ended without an empty line at the end
        yield tuple(sent)

def test_read_sentences():
    f = StringIO(u"1_1\tIch\t_\t_\t_\t_\t_\t_\t0\t_\t_\t_\t_\t_\t_\n"
                 "1_2\tsehe\t_\t_\t_\t_\t_\t_\t0\t_\t_\t_\t_\t_\t_\n"
                 "1_3\t.\t_\t_\t_\t_\t_\t_\t0\t_\t_\t_\t_\t_\t_\n"
                 " \n "
                 "2_1\tIch\t_\t_\t_\t_\t_\t_\t0\t_\t_\t_\t_\t_\t_\n"
                 "2_2\tsehe\t_\t_\t_\t_\t_\t_\t0\t_\t_\t_\t_\t_\t_\n"
                 "2_3\tihn\t_\t_\t_\t_\t_\t_\t0\t_\t_\t_\t_\t_\t_\n"
                 "2_4\t.\t_\t_\t_\t_\t_\t_\t0\t_\t_\t_\t_\t_\t_\n")
    assert list(read_sentences(f)) == [S_1, S_2]


## Sentence -> String
def s2string(s):
    """Produce a one-line string with forms from s."""
    return " ".join(token.form for token in s)

def test_s2string():
    assert s2string(S_0) == ""
    assert s2string(S_1) == "Ich sehe ."
    assert s2string(S_2) == "Ich sehe ihn ."


## Sentence -> String
def s2conll09(s):
    """Produce a string representing the sentence in conll09 format."""
    string = ""
    for token in s:
        string += "\t".join(token) + "\n"
    return string

def test_s2conll09():
    assert s2conll09(S_0) == ""
    assert s2conll09(S_1) == "1_1\tIch\t_\t_\t_\t_\t_\t_\t0\t_\t_\t_\t_\t_\t_\n" \
                           "1_2\tsehe\t_\t_\t_\t_\t_\t_\t0\t_\t_\t_\t_\t_\t_\n" \
                           "1_3\t.\t_\t_\t_\t_\t_\t_\t0\t_\t_\t_\t_\t_\t_\n"


## Sentence -> String
def s2conllX(s):
    """Produce a string representing the sentence in conll-X format."""
    string = ""
    for token in s:
        string += "\t".join([token.id, token.form, token.lemma, "_",
                             token.pos, token.feat, token.head, token.deprel,
                             token.phead, token.pdeprel]) + "\n"
    print(string)
    return string

def test_s2conllX():
    assert s2conllX(S_0) == ""
    assert s2conllX(S_7) == \
           "1_1\tIch\tich\t_\tPPER\tnom|sg|*|1\t2\tSB\t_\t_\n" \
           "1_2\tlese\tlesen\t_\tVVFIN\tsg|1|pres|ind\t0\t--\t_\t_\n" \
           "1_3\t.\t--\t_\t$.\t_\t2\t--\t_\t_\n"


## Sentence -> String
def get_id(s):
    """Return sentence id.
    ASSUME: - 'id' field of tokens is of the form "number_number", where the first number is sentence id
            - sentence id of all tokens in the sentence is the same
    """
    if not s:
        return ""
    else:
        return s[0].id.split('_')[0]

def test_get_id():
    assert get_id(S_0) == ""
    assert get_id(S_1) == "1"
    assert get_id(S_2) == "2"
