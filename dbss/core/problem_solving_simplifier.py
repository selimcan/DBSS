#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from dbss import config
from dbss.lib.aima_python_fork import search
from dbss.core import sentence, simplifier

## A problem-solving-agent-based sentence simplifier.
##
## UNIT TESTS: python3 -m pytest problem_solving_simplifier.py
##             (py.test has to be installed)


## =================
## Data definitions:


## Action is (tupleof Callable, (listof Any))
## interp. a function to apply to a sentence to produce a new sentence
## along with arguments to it.

A_1 = (simplifier.remove_t_with_id, ['1_1'])


class SentenceSimplificationProblem(search.Problem):
    """"State are Sentences. goal_test returns True for any sentence so that all
    candidates get generated.
    """

    ## Sentence -> (listof Action)
    def actions(self, sentence):
        """Return the actions that can be executed in the given
        state. The result would typically be a list, but if there are
        many actions, consider yielding them one at a time in an
        iterator, rather than building them all at once."""
        return [(simplifier.remove_t_with_id, [t.id, sentence]) for t in sentence]

    ## Sentence Action -> Sentence
    def result(self, sentence, action):
        """Return the state that results from executing the given
        action in the given state. The action must be one of
        self.actions(state)."""
        func, args = action
        return func(*args)

    ## Sentence -> Bool
    def goal_test(self, sentence):
        """Return True if the state is a goal. The default method compares the
        state to self.goal or checks for state in self.goal if it is a
        list, as specified in the constructor. Override this method if
        checking against a single self.goal is not enough."""
        return True

    ## (union Int Float) Sentence Action Sentence
    def path_cost(self, c, state1, action, state2):
        """Return the cost of a solution path that arrives at state2 from
        state1 via action, assuming cost c to get up to state1. If the problem
        is such that the path doesn't matter, this function will only look at
        state2.  If the path does matter, it will consider c and maybe state1
        and action. The default method costs 1 for every step in the path."""
        return c + 1


## ==========
## Functions:


## Sentence Integer -> (generator Sentence)
def simplify(s, n):
    """Generate up to n simpler versions of s."""
    ssp = SentenceSimplificationProblem(s)
    for goal in simplifier.take_n(n, search.breadth_first_search(ssp)):
        yield goal.state

def test_simplify():
    assert list(simplify(sentence.S_0, config.DEFAULT_MAX_NUMBER_OF_CANDIDATES)) == [()]
    assert list(map(sentence.s2string, simplify(sentence.S_1, config.DEFAULT_MAX_NUMBER_OF_CANDIDATES))) ==\
           ["Ich sehe .", "sehe .", "Ich .", "Ich sehe",
            ".", "sehe", "Ich",
            ""]
    assert list(map(sentence.s2string, simplify(sentence.S_1, config.DEFAULT_MAX_NUMBER_OF_CANDIDATES))) ==\
           ["Ich sehe .", "sehe .", "Ich .", "Ich sehe", '.', 'sehe', 'Ich', '']