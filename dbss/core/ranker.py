#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import abc

## Score simplified candidates according to some metric in dbss/plugins/ranker.


class Ranker(object):
    __metaclass__ = abc.ABCMeta

    ## (listof/generator (Sentence, Sentence, (listof String))) -> (generator (Sentence, Sentence, Float))
    @abc.abstractmethod
    def score(self, original, simplified, actions):
        """"
        Given a list of (original sentence, simplified variant, actions taken to simplify) tuples,
        return (original, simplified, score for the simplified).
        """
        return  # original, simplified, score
