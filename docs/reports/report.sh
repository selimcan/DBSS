#!/usr/bin/env bash

declare -a arr=("1_OneSubtreeShorter" "2_10Shortest+ShortestWithoutPunctuation" "3_CombinationOf1And2" "4_10Longest+LongestWithoutPunctuation" "5_CombinationOf1And4" "6_OneTokenShorter" "7_10WithLeastParsableSubtreesRemoved+FirstCandidateWithoutPunctuation")

bash experiments/eval.sh
printf "\n"

for exp in "${arr[@]}"
do
    printf "Experiment: $exp\n"
    ./src/evaluator -i experiments/input/tiger_train_easyPunct_xleFail.conll09 -a dummy \
    -e experiments/"$exp"/Candidates/ -p experiments/"$exp"/ParsedSentences/ -l experiments/"$exp"/log.txt
    printf "\n--------------------------------------------\n"
done
