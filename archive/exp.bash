#!/usr/bin/env bash

python3 -u dbss/use_cases/persistent_parser.py -i "$1" -c data/experiments/validate10WithoutLeastParsable/Candidates/ -p data/experiments/validates10WithoutLeastParsable/XLEParses/ -o data/experiments/validate10WithoutLeastParsable/ParsedSentences/ -m OneSubtreeShorterRecursive+FirstWithoutPunctuation -s GreedyByParsability=../ozlem/research/GermanLFG/ErrorAnalysis/tiger_train_punctCoord_xleFail.123gramsParsability --max_solutions 1 >> data/experiments/validate10WithoutLeastParsable/log.txt

