#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys

from dbss import config
from dbss.core import sentence


## Look into CANDIDATE_SENTS and PARSED_SENTS subfolders of experiment-results-folders
## listed in EXPS_TO_CONSIDER,
## and output two files, PARSED_CONLL and FAILED_CONLL with unique sentences which were
## fully parsed and were NOT fully parsed by XLE, respectively.
##
## ASSUME: CANDIDATE_SENTS folder: for each original sentence with id X in the input conll09 file in stdin,
## it contains a conll09 file named X.conll09 with all simplified candidates
## generated in the experiment for the sentence X.
##
## ASSUME: PARSED_SENTS folder: for each original sentence X in input conll file in stdin,
## contains those simplified candidates which were fully parsed by XLE.
##
## USAGE: cat input.conll | ./experiments2dataset.py <experiments-folder>


## =================
## Constants:


CANDIDATE_SENTS = 'Candidates'
PARSED_SENTS = 'ParsedSentences'
EXPS_TO_CONSIDER = ['1_OneSubtreeShorter',
                    '2_10Shortest+ShortestWithoutPunctuation',
                    '4_10Longest+LongestWithoutPunctuation',
                    '6_OneTokenShorter',
                    '7_10WithLeastParsableSubtreesRemoved+FirstCandidateWithoutPunctuation']
PARSED_CONLL = 'parsed' + config.CONLL_EXTENSION
FAILED_CONLL = 'failed' + config.CONLL_EXTENSION


## =================
## Functions:


def main(exps_folder):
    SEEN = set()

    with open(PARSED_CONLL, 'w') as pcf, open(FAILED_CONLL, 'w') as fcf:
        for s in sentence.read_sentences(sys.stdin):
            for exp in EXPS_TO_CONSIDER:

                parsed_sents_file = os.path.join(exps_folder, exp, PARSED_SENTS, sentence.get_id(s) +
                                                 config.CONLL_EXTENSION)
                with open(parsed_sents_file) as psf:
                    parsed = {sentence.s2string(s) for s in sentence.read_sentences(psf)}

                candidate_sents_file = os.path.join(exps_folder, exp, CANDIDATE_SENTS, sentence.get_id(s) +
                                                    config.CONLL_EXTENSION)
                with open(candidate_sents_file) as csf:
                    for candidate in sentence.read_sentences(csf):
                        plain_c = sentence.s2string(candidate)
                        if plain_c not in SEEN:
                            SEEN.add(plain_c)
                            if plain_c in parsed:
                                pcf.write(sentence.s2conll09(candidate) + '\n')
                            else:
                                fcf.write(sentence.s2conll09(candidate) + '\n')

if __name__ == '__main__':
    main(sys.argv[1])
