#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import io
import re

## Turns an .lfg.new file of the following form:
##     # SENTENCE_ID: 2007
##
##     Ein Gastgeber , der kein Lehrmeister mehr ist (*1+6159 0.11 1265)
##
##     # SENTENCE_ID: 2182
##
##     Portugals KP-Chef geht bald (2 0.05 77)
## into a tsv file:
##     2007\tEin Gastgeber , der kein Lehrmeister mehr ist\t(*1+6159 0.11 1265)
##     2182\tPortugals KP-Chef geht bald\t(2 0.05 77)

input_stream = io.TextIOWrapper(sys.stdin.buffer, encoding='ISO-8859-1')

for line in input_stream:
    if line.strip():
        if 'SENTENCE_ID' in line:
            print(re.search("SENTENCE_ID: (\d+)", line.strip()).group(1), end='')
            print("\t", end='')
        else:
            sent_solution = re.search("(.*) (\([-+*.E0-9]+ [-+*.E0-9]+ [-+*.E0-9]+\))$", line)
            try:
                print(sent_solution.group(1), end='')
                print("\t", end='')
                print(sent_solution.group(2))
            except AttributeError:
                print(line.strip())
