clean:
	find . -name "*.pyc" -delete
	find . -type d -name "*__pycache__" -delete
	rm -rf dbss/.cache
test:
	./unit_tests/run_all.sh
	./acceptance_tests/run_all.sh
	behave

