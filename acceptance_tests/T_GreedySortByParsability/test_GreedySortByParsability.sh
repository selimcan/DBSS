#!/usr/bin/bash

# Feature: when generating candidates, delete least parsable elements (tokens
#          or subtrees) first.
#          In case there are collisions in parsability scores, whichever element
#          is closer to the beginning of the sentence gets deleted first.
#
# "Greedy" since candidates provided by only one recursion level of a simpler
# sentence generator are sorted:
#                                "a b c ."
#                                   |
#                 sort("b c .", "a c .", "a b .", "a b c")
#                         |
#            sort("c .", "b .", "b c")           etc.
# "Non-greedy" or "optimal" sort would be to generate all candidates up-front
# and sort them:
#                                "a b c ."
#                                   |
#        sort(         "b c .", "a c .", "a b .", "a b c"
#                         |
#               "c .", "b .", "b c"     etc.                  )
# In case of non-recursive generation (i.e. recursion level=1), there is no difference.


all_scenarios_passed=true


# Scenario1: OneTokenShorterRecursiveBFSearch mode, parsabilities.txt contains parsability scores for all
#            of the deleted tokens (1-grams), and scores are all different (so that sorting is deterministic).
  # Given
    rm -rf Candidates/ XLEParses/ ParsedSentences/
  # When
    python3 dbss/use_cases/persistent_parser.py -i acceptance_tests/T_GreedySortByParsability/Scenario1/input.conll09 \
    -c Candidates/ -p XLEParses/ -o ParsedSentences/ -m OneTokenShorterRecursiveBFSearch \
    -s GreedyByParsability=acceptance_tests/T_GreedySortByParsability/Scenario1/parsabilities.txt
  # Then
    if diff acceptance_tests/T_GreedySortByParsability/Scenario1/expected_candidates/4.conll09 Candidates/4.conll09 && \
       diff acceptance_tests/T_GreedySortByParsability/Scenario1/expected_candidates/4.lfg Candidates/4.lfg;
    then
      echo "     PASSED: Scenario 1 of $0"
      rm -rf Candidates/ XLEParses/ ParsedSentences/
    else
      all_scenarios_passed=false
      echo "     FAILED: Scenario 1 of $0"
      rm -rf Candidates/ XLEParses/ ParsedSentences/
    fi


# Scenario2: OneTokenShorterRecursiveBFSearch mode, parsabilities.txt does not contain a score for one of
#            the deleted tokens (1-grams), so that its parsability score is set to 1.0 (highest possible value),
#            and sentences not containing it are tried last.
  # Given
    rm -rf Candidates/ XLEParses/ ParsedSentences/
  # When
    python3 dbss/use_cases/persistent_parser.py -i acceptance_tests/T_GreedySortByParsability/Scenario2/input.conll09 \
    -c Candidates/ -p XLEParses/ -o ParsedSentences/ -m OneTokenShorterRecursiveBFSearch \
    -s GreedyByParsability=acceptance_tests/T_GreedySortByParsability/Scenario2/parsabilities.txt
  # Then
    if diff acceptance_tests/T_GreedySortByParsability/Scenario2/expected_candidates/4.conll09 Candidates/4.conll09 && \
       diff acceptance_tests/T_GreedySortByParsability/Scenario2/expected_candidates/4.lfg Candidates/4.lfg;
    then
      echo "     PASSED: Scenario 2 of $0"
      rm -rf Candidates/ XLEParses/ ParsedSentences/
    else
      all_scenarios_passed=false
      echo "     FAILED: Scenario 2 of $0"
      rm -rf Candidates/ XLEParses/ ParsedSentences/
    fi


# Scenario3: OneTokenShorterRecursiveBFSearch mode, parsabilities.txt does not contain scores for several of
#            the deleted tokens (1-grams), in which case their parsability scores are set to 1.0 (highest possible
#            value), and sentences not containing them are tried last.
  # Given
    rm -rf Candidates/ XLEParses/ ParsedSentences/
  # When
    python3 dbss/use_cases/persistent_parser.py -i acceptance_tests/T_GreedySortByParsability/Scenario3/input.conll09 \
    -c Candidates/ -p XLEParses/ -o ParsedSentences/ -m OneTokenShorterRecursiveBFSearch \
    -s GreedyByParsability=acceptance_tests/T_GreedySortByParsability/Scenario3/parsabilities.txt
  # Then
    if diff acceptance_tests/T_GreedySortByParsability/Scenario3/expected_candidates/4.conll09 Candidates/4.conll09 && \
       diff acceptance_tests/T_GreedySortByParsability/Scenario3/expected_candidates/4.lfg Candidates/4.lfg;
    then
      echo "     PASSED: Scenario 3 of $0"
      rm -rf Candidates/ XLEParses/ ParsedSentences/
    else
      all_scenarios_passed=false
      echo "     FAILED: Scenario 3 of $0"
      rm -rf Candidates/ XLEParses/ ParsedSentences/
    fi


# Scenario4: OneSubtreeShorterNonRecursive mode, parsabilities.txt contains scores for all
#            of the deleted n-grams.
  # Given
    rm -rf Candidates/ XLEParses/ ParsedSentences/
  # When
    python3 dbss/use_cases/persistent_parser.py -i acceptance_tests/T_GreedySortByParsability/Scenario4/input.conll09 \
    -c Candidates/ -p XLEParses/ -o ParsedSentences/ -m OneSubtreeShorterNonRecursive \
    -s GreedyByParsability=acceptance_tests/T_GreedySortByParsability/Scenario4/parsabilities.txt
  # Then
    if diff acceptance_tests/T_GreedySortByParsability/Scenario4/expected_candidates/880.conll09 Candidates/880.conll09 && \
       diff acceptance_tests/T_GreedySortByParsability/Scenario4/expected_candidates/880.lfg Candidates/880.lfg;
    then
      echo "     PASSED: Scenario 4 of $0"
      rm -rf Candidates/ XLEParses/ ParsedSentences/
    else
      all_scenarios_passed=false
      echo "     FAILED: Scenario 4 of $0"
      rm -rf Candidates/ XLEParses/ ParsedSentences/
    fi


# Scenario5: OneSubtreeShorterNonRecursive mode, parsabilities.txt does not contain a score for some of
#            the deleted n-grams where n>1, in which case their parsability is defined as the minimum of
#            parsability scores of [n-1]-grams they contain (parsability of an unknown 1-gram = 1.0, as
#            in the above scenarios).
  # Given
    rm -rf Candidates/ XLEParses/ ParsedSentences/
  # When
    python3 dbss/use_cases/persistent_parser.py -i acceptance_tests/T_GreedySortByParsability/Scenario5/input.conll09 \
    -c Candidates/ -p XLEParses/ -o ParsedSentences/ -m OneSubtreeShorterNonRecursive \
    -s GreedyByParsability=acceptance_tests/T_GreedySortByParsability/Scenario5/parsabilities.txt
  # Then
    if diff acceptance_tests/T_GreedySortByParsability/Scenario5/expected_candidates/880.conll09 Candidates/880.conll09 && \
       diff acceptance_tests/T_GreedySortByParsability/Scenario5/expected_candidates/880.lfg Candidates/880.lfg;
    then
      echo "     PASSED: Scenario 5 of $0"
      rm -rf Candidates/ XLEParses/ ParsedSentences/
    else
      all_scenarios_passed=false
      echo "     FAILED: Scenario 5 of $0"
      rm -rf Candidates/ XLEParses/ ParsedSentences/
    fi


# TODO
# Scenario6: OneSubtreeShorterNonRecursive mode, parsabilities.txt does not contain a score for some of
#            the deleted n-grams where n>1, but also no scores for any of the lower order n-grams.


if [ "$all_scenarios_passed" = true ]; then
    echo "PASSED: $0"
    exit 0;
else
    echo "FAILED: $0"
    exit 1
fi
